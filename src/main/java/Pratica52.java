
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {

    public static void main(String[] args) {

        //Raizes diferentes
        try {
            Equacao2Grau<Integer> e = new Equacao2Grau<>(1, 3, -4);
            System.out.println("Raizes diferentes");
            System.out.println("Raiz1: " + e.getRaiz1());
            System.out.println("Raiz2: " + e.getRaiz2());
        } catch (RuntimeException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

        //Raizes iguais
        try {
            Equacao2Grau<Integer> e = new Equacao2Grau<>(1, 2, 1);
            System.out.println("Raizes iguais");
            System.out.println("Raiz1: " + e.getRaiz1());
            System.out.println("Raiz2: " + e.getRaiz2());
        } catch (RuntimeException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

        //Não há raizes
        try {
            Equacao2Grau<Integer> e = new Equacao2Grau<>(1, -2, 16);
            System.out.println("Raiz1: " + e.getRaiz1());
            System.out.println("Raiz2: " + e.getRaiz2());
        } catch (RuntimeException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }
}
