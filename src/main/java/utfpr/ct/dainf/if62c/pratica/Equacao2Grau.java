/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author root
 */
public class Equacao2Grau <T extends Number> {
    
    private T a;
    private T b;
    private T c;
    
    private double x1;
    private double x2;    

    public T getA() {
        return a;
    }

    public void setA(T a) {
        if (a.doubleValue() == 0)
            throw new RuntimeException("Coeficiente a não pode ser zero");
        else
            this.a = a;
    }

    public void setB(T b) {
        this.b = b;
    }

    public void setC(T c) {
        this.c = c;
    }

    public T getB() {
        return b;
    }

    public T getC() {
        return c;
    }
    
    public Equacao2Grau(T a, T b, T c){
        if (a.doubleValue() == 0)
            throw new RuntimeException("Coeficiente a não pode ser zero");
        else
            this.a=a;
        this.b=b;
        this.c=c;
    }
    
    public double getRaiz1(){
        
        if ((Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue()) < 0)
            throw new RuntimeException("Equação não tem solução real");
        else
            this.x1 = (-this.b.doubleValue() + Math.sqrt(Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue()))/2*this.a.doubleValue();
        
        return x1;
    }
    
    public double getRaiz2(){
        
        if ((Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue()) < 0)
            throw new RuntimeException("Equação não tem solução real");
        else
            this.x2 = (-this.b.doubleValue() - Math.sqrt(Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue()))/2*this.a.doubleValue();
        
        return this.x2;
    }
    
}
